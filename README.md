# What is Kolben Web ?
Kolben is a collection of free software and tools to help you get started on your own.

It's also the base upon which sites like [kolbengames.com](http://kolbengames.com) and [Nexar.ca](http://nexar.ca) are built.

# What's it made from ?
Kolben Web uses cool stuff like:
-
- Yeoman from [yeoman.io](http://yeoman.io/)
- Bootstrap from [getbootstrap.com](http://getbootstrap.com/)
- NodeJS from [nodejs.org](http://nodejs.org/)
- jQuery from [jquery.com](http://jquery.com/)
- Sass from [sass-lang.com](http://sass-lang.com/)
- Compass from [compass-style.org](http://compass-style.org/)
And much more ! If you're curious, take a look here: [Kolben Web Dev](http://kolbengames.com/dev)

# Kolben Web 1.3.0 Roadmap.
### TODO:
- Implement Mini Games section.
- Move _play.html_ design to PHP.
- Generate game pages automatically.
- Find clever version name. ("Mountains ?")

### DONE:
- Mini Games shelf design.
- Game page design.
- Comment system.

### FIXME:
- Share section in Game page.
- "New" tag in version posts.