<?php
	if(empty($_POST)){
		include('submit.html');
	} else {
		$name = $_POST['Name'];
		$title = $_POST['Title'];
		$mail = $_POST['Email'];
		$msg = $_POST['Message'];
		$link = $_POST['Link'];
		if(filter_var($mail, FILTER_VALIDATE_EMAIL) == false){
			include('submit.html');
		} else {
			$msg_format = 'Developer: ' . $name . '
Game Title: ' . $title . '
Mail: ' . $mail . '
Message: ' . $msg . '
Link: ' . $link;
			$header = 'From: ' . $mail;
			mail('bladetecltd@gmail.com', 'Game Submission', $msg_format, $header);
			include('submitted.html');
		}
	}
?>